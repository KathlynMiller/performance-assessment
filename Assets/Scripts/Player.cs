﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	float speed = 5.5f;
	
	void Update() {
		var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
		transform.position += move * speed * Time.deltaTime;
	}

}